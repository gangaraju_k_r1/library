package com.ty.library.libraryappboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryAppBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryAppBootApplication.class, args);
	}

}
